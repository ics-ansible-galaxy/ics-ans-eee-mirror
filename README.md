ics-ans-eee-mirror
==================

Ansible playbook to install an EEE server mirror (EPICS base, modules and startup directories).

*/export/epics* and */export/startup* are exported via NFS in read-only mode.
*/export/nonvolatile* can be used to write data.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
