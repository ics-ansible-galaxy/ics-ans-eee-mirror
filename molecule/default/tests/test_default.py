import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('eee_mirror')


def test_eee_local_directories(host):
    for item in ('/export/epics/modules',
                 '/export/startup/boot'):
        directory = host.file(item)
        assert directory.is_directory
        assert directory.user == 'ess'


@pytest.mark.parametrize('service', [
    'nfs-server',
    'rpcbind',
    'rsync-epics.service',
    'rsync-startup.service'])
def test_service_is_enabled(host, service):
    host.service(service).is_enabled


def test_rsync_options(host):
    rsync_startup = host.file('/etc/systemd/system/rsync-startup.service')
    assert 'rsync -v --delete --recursive --links --perms --times --timeout 120' in rsync_startup.content_string
